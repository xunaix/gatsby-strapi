import React from "react"
import { graphql } from "gatsby"

const Page = (props) => {
  const { seo, components, title } = props.data.strapiPage
  return (
    <div>
      <h1>{title}</h1>
      <h2>seo</h2>
      <p>{seo.description}</p>
      <img src={seo.image.url} alt={seo.image.alternativeText} />
      <p>sss</p>
      <h2>components</h2>
      {components.map((item, index) => {
        return (
          <div key={index}>
            {item.title && <p>{item.title}</p>}
            {item.subtitle && <p>{item.subtitle}</p>}
            {item.position && <p>{item.position ? "true" : "nie true"}</p>}
            {item.image && (
              <img src={item.image.url} alt={item.image.alternativeText} />
            )}
          </div>
        )
      })}
      {console.log({ seo })}
      {console.log({ components })}
    </div>
  )
}

export const pageQuery = graphql`
  query($path: String!) {
    strapiPage(url: { eq: $path }) {
      title
      seo {
        description
        image {
          url
          alternativeText
        }
      }
      components {
        title
        subtitle
        position
        image {
          alternativeText
          url
        }
      }
    }
  }
`

export default Page
